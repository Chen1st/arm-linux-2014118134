# 　韩山师范学院实验报告　 
### 姓名： 陈智聪　专业：软件工程
### 学号 : 2014118134　　　科目：嵌入式 Linux-ARM 应用开发　
###实验日期：2017.4.20
***
## 实验目的 ：
1. 学会使用git和sourcetree 
2. 让代码进行更好的版本管理 

## 实验内容 ：
1. 用韩师邮箱注册bitbucket 账号并建立仓库
2. 建立team
3. Source本地克隆和云端

### 账号注册

![账号注册][1]

### bitbucket 云端仓库
![bitbucket 仓库][2]

### sourceTree 克隆仓库
![sourceTree 克隆仓库][3]


  [1]:https://bytebucket.org/Chen1st/arm-linux-2014118134/raw/d99eb06e2518bc1982cfc06d96aa93eb39e142fb/%E5%AE%9E%E9%AA%8C%E4%B8%80/%E8%B4%A6%E5%8F%B7%E6%B3%A8%E5%86%8C.png
  [2]:https://bytebucket.org/Chen1st/arm-linux-2014118134/raw/d99eb06e2518bc1982cfc06d96aa93eb39e142fb/%E5%AE%9E%E9%AA%8C%E4%B8%80/%E4%BA%91%E7%AB%AF%E4%BB%93%E5%BA%93.png
  [3]: https://bytebucket.org/Chen1st/arm-linux-2014118134/raw/d99eb06e2518bc1982cfc06d96aa93eb39e142fb/%E5%AE%9E%E9%AA%8C%E4%B8%80/%E6%9C%AC%E5%9C%B0%E5%85%8B%E9%9A%86%E4%BB%93%E5%BA%93.png